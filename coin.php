<?php
include 'header.php';
$response_golds = $client->request('GET', 'getGoldList', [
    'headers' => [
        'Authorization' => $user['apikey']
    ],
]);
$coins = json_decode($response_golds->getBody(), true);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >


    <div class="container-fluid  p-xl-3 p-2" id="div_messages">

        <p class="gold_btn d-inline-block"><?php echo "موجودی سکه: " . $coins["inventory"] . " عدد"?></p>


        <div class="row ">
            <div class="col-lg-8 col-md-6  mr-auto ml-auto">




                <div class="d-flex flex-column">

                    <span class="align-self-center ml-2 text-bold">دعوت از دوستان</span>

                    <span>کاربر گرامی شما میتوانید با دعوت از دوستان خود و ورود آنها به پوشکا از طریق کد معرف شما 10 سکه هدیه  بگیرید </span>

                    <div class="mt-2 d-flex align-items-center">
                        <span class="my_card pl-4 pr-4 pt-2 pb-2"><?php echo "کد کاربری : " . $user["user_code"]?></span>
                        <span class="span_copy pointer" id="span_copy">کپی کردن</span>
                        <span class="mr-2" id="lbl_copy">کد کاربری ذخیره شد</span>

                    </div>

                    <hr class="w-100 my_background"/>

                </div>


                <?php
                foreach ($coins["golds"] as $coin) {
                    $dis_class = $coin["dis"] > 0 ? "my-red text-bold ml-1" : "my-red text-bold ml-1 v-hidden";
                    $dicsoucnted_price = number_format($coin['price'] - ($coin['price'] * $coin['dis'] / 100)) . " تومان ";
                    $price_class = $coin["dis"] > 0 ? "mr-2 my-red strikethrough" : "mr-2";
                    $red_price_class = $coin["dis"] > 0 ? "mr-2 my-green" : "mr-2 my-red v-hidden";

                    $price = number_format($coin["price"]). " تومان";

                    echo "          <div class=\"d-flex flex-column\">
                   <div class=\"d-flex justify-content-between\">
                       <div></div>
                       <span class=\"text-bold \">$coin[title]</span>
                      
                       <span class=\"$dis_class\"> <i class=\"fa small-font fa-percent\"></i> $coin[dis] </span>
                   </div>
                      <div>
                          <img class=\"mr-2\" src=\"assets/coin.svg\" width=\"28\" height=\"28\">
                          <span class=\"text-bold mt-2\">$coin[cnt] عدد </span>
                      </div>
                      <span class=\"mr-2 mt-2\">$coin[des]</span>

                      <div class=\"mt-2 d-flex align-items-center\">
                          <span class=\"$price_class\"> $price </span>
                          <span class=\"$red_price_class\">$dicsoucnted_price </span>
                          <button id='$coin[gold_id]' class=\"mr-auto btn btn-success btn-gold\"> <i class=\"fa fa-shopping-cart\"></i> خرید بسته</button>
                      </div>

                      <hr  class=\"w-100 my_background\"/>

                  </div>";
                }
                ?>


            </div>

        </div>

    </div><!-- /.container-fluid -->

</div>

<!-- /.content -->


<!-- ./wrapper -->


<!-- jQuery -->
<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="dashbord/dist/js/exit.js"></script>
<script src="js/compress.js"></script>

<script>

    $(document).ready(function () {
        $('#lbl_copy').hide();
        var initial = "<?php echo $user["username"] ?>";
        var apikey = "<?php echo $user['apikey']?>";
        var myFormData = new FormData();

        $('.btn-gold').click(function () {
            var id = $(this).attr('id');
            var user_id =  <?php echo $user["user_id"]; ?>;
            var apikey =  "<?php echo $user["apikey"]; ?>";
            var aow = "web";
            $.ajax({
                type: "post",
                url: baseURl + "goldReq",
                data: {
                    'gold_id':id,
                     'aow':aow
                },
                headers: {
                    "Authorization": apikey
                },
                oncomplete: function () {

                },

                success: function (result, status, xhr) {
                    var data = result["message"];
                    var url = baseURl+"goldPur/"+user_id + "?data="+data
                          window.location = url;

                },

                error: function (xhr, status, error) {
                    console.log(xhr.responseText)

                }


            })




        })

        $('#profile-img').click(function () {
            $('#img-input').trigger('click');
        })

        $('#btn_save').click(function () {
            var username_changed = false;
            var name = isEmpty($('#et_name').val().toString()) ? null : $('#et_name').val()
            var username = isEmpty($('#et_username').val()) ? null : $('#et_username').val()
            var bio = isEmpty($('#et_bio').val()) ? null : $('#et_bio').val()
            var t_id = isEmpty($('#et_telegram').val()) ? null : $('#et_telegram').val()
            var instagram = isEmpty($('#et_instagram').val()) ? null : $('#et_instagram').val()
            var email = isEmpty($('#et_email').val()) ? null : $('#et_email').val()

            if (username == null) {
                username_changed = false;
            } else {
                if (initial.toUpperCase() === username.toUpperCase()) {
                    username_changed = false
                } else {
                    username_changed = true;
                }


            }

            var yourObject = {
                huc: username_changed,
                username: username,
                name: name,
                email: email,
                t_id: t_id,
                instagram: instagram,
                bio: bio,
                sex: 1
            }
            myFormData.append('object', JSON.stringify(yourObject));


            showLoading(true)
            $.ajax({
                type: "post",
                processData: false, // important
                contentType: false, // important
                dataType: 'json',
                url: baseURl + "infoUpdate",
                data: myFormData,
                headers: {
                    "Authorization": apikey
                },
                oncomplete: function () {
                    alert("salam")
                    showLoading(false)
                },

                success: function (result, status, xhr) {

                    showLoading(false)
                    if (result["thumb"] != null) {
                        $('#img_top').attr('src', IMAGE_URL + "profile/" + result["thumb"])
                    }

                    showError(true, true, "حساب کاربری شما به روز گردید")

                },

                error: function (xhr, status, error) {
                    console.log(xhr.responseText)
                    var json = JSON.parse(xhr.responseText);
                    showLoading(false)
                    showError(true, false, json["message"])

                }


            })


        })
        //handling what happens after img-input is selected
        $("#img-input").change(function () {
            //  var formData = new FormData(this);
            readURL(this);
        });

        $('#span_copy').click(function () {
            var user_code = "<?php echo $user["user_code"]?>";
            var dummy = $('<input>').val(user_code).appendTo('body').select()
            document.execCommand('copy')
            dummy.remove();

            $('#lbl_copy').show();
              setTimeout(function () {
                  $('#lbl_copy').hide(500);
              },1500);



        })

        function isEmpty(value) {
            return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                    // invalid file type code goes here.


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#profile-img').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                    form_data = input.files[0];

                    //  myFormData.append('pic', input.files[0])

                    new ImageCompressor(input.files[0], {
                        quality: .8,
                        success(result) {

                            myFormData.append('pic', result, result.name)
                            // Send the compressed image file to server with XMLHttpRequest.
                        },
                        error(e) {
                            console.log(e.message);
                        },
                    });


                } else {
                    swal({
                        title: "خطا !",
                        text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }


            }
        }


        function showLoading(show) {
            if (show) {
                $('#btn_save').html("  درحال ذخیره سازی\n" +
                    "                         <span class=\"spinner-border spinner-border-sm mr-2\"></span>\n")
            } else {
                $('#btn_save').html("ذخیره تغییرات")
            }
        }

        function showError(show, success, message)  {
            if (show) {
                if (success) {
                    $('#error_div').removeClass('d-none btn-danger').addClass(' btn-success').html(message)
                } else {
                    $('#error_div').removeClass('d-none btn-success').addClass(' btn-danger').html(message)
                }

            } else {
                $('#error_div').removeClass('d-none')
            }
        }


    })

</script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>
