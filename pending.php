<?php
    include 'header.php';
    use Src\helper\Practical;
  //  include '../helper/jdf.php';
    $response_pending = $client->request('GET', 'pendingUsers', [
        'headers' => [
            'Authorization' => $token
        ],
    ]);
    $response_pending = json_decode($response_pending->getBody(), true);

    $pendings = $response_pending["data"]["pending"];
    $pendings_documents = $response_pending["data"]["document"];
    ?>

    <div class="content-wrapper">
        <div class="container-fluid p-xl-3 p-2" id="div_messages">

            <div class="row m-0" >

                <div class="col-lg-6  mt-2 rtl text-center p-0 pl-md-2 pr-md-2"
                >
                    <ul class="w-100 p-0 mb-0 tabs text-bold  d-inline-flex align-self-start ">
                        <li class="current" data-tab="tab-1">در انتظار تایید
                            <span class="right badge badge-danger"> <?php echo count($pendings)
                                ?> </span>
                        </li>
                        <li class="" data-tab="tab-2"> در انتظار مدرک
                            <span class="right badge badge-danger"> <?php echo count($pendings_documents)
                                ?> </span>
                        </li>
                    </ul> <!--tabs ul and lis-->
                </div>
            </div>

            <div id="tab-1" class="tab-content current p-0 ">
            <div class="row mt-3">
            <?php
              if(count($pendings)==0) {
                 echo Practical::printEmpty("لبست کاربران در انتظار خالی میباشد");
              }else {
                  foreach ($pendings as $user) {
                      $karfarma = karfarma_function($user);
                      echo "  
                  <div class=\"col-lg-6 \" id='div_$user[user_id]'>
                  <div class='flex-column d-flex my_card mt-1 p-2 '>
                   <div class='d-flex mt-3 justify-content-between'>
                   <span class='text-bold'>فرشگاه : $user[store_name]</span>
                   <span style='direction: ltr'>$user[shamsi]</span>
                 </div>  
                 <hr style='  height: 2px; width: 100%'/>
                 
                  <div class='d-flex  justify-content-between'>
                   <span>$user[name] $user[family]</span>
                   <span class='text-blue'>$user[mobile] <i class='fa fa-mobile-alt'></i></span>
                  </div>
                  
                     <div class='d-flex mt-2'>
                   <span class='text-gray'>صنف : </span>
                   <span class='mr-2'>$user[guild_name]</span>
                  </div>
                  <div class='d-flex mt-2'>
                   <span class='text-gray'>موقعیت مکانی : </span>
                   <span class='mr-2'>$user[province_name] / $user[city_name]</span>
                  </div>
                     <div class='d-flex mt-2'>
                   <span class='text-gray'>آدرس : </span>
                   <span class='mr-2'>$user[address]</span>
                  </div>
                  $karfarma
                 
                  <div class='d-flex mt-2 justify-content-end'>
                   <button data-id = '$user[user_id]' class='btn btn-success small-font btn-confirm'>تایید کردن</button>
                   <button data-id = '$user[user_id]' class='btn btn-success mr-2 small-font btn-document'>درخواست مدرک</button>
                   <button data-id = '$user[user_id]' class='btn btn-danger mr-2 small-font btn-reject'>رد کردن</button>
                  </div>
               </div>
        
            </div>";
                  }
              }

            ?>
            </div>
            </div>
<!--            document pendings-->
            <div id="tab-2" class="tab-content p-0">
                <div class="row mt-3">
                    <?php
                    if(count($pendings_documents)==0) {
                        echo Practical::printEmpty("لبست کاربران در انتظار خالی میباشد");
                    }else {
                        foreach ($pendings_documents as $user) {
                            $document = $user["document"];
                            if ($document==null) {
                                $document_span = "<span class='text-danger document text-bold' data-document='$document'>  آپلود نشده </span>";
                            }else {
                                $document_span = "<span class='text-success document pointer text-bold' data-document='$document'>  مشاهده مدرک </span>";
                            }
                            $karfarma = karfarma_function($user);
                            echo "  
                  <div class=\"col-lg-6 \" id='div_$user[user_id]'>
                  <div class='flex-column d-flex my_card mt-1 p-2 '>
                   <div class='d-flex mt-3 justify-content-between'>
                   <span class=''>فرشگاه : $user[store_name]</span>
                   <span style='direction: ltr'>$user[shamsi]</span> 
                 </div>  
                 $document_span
                 <hr style='height: 2px; width: 100%'/>
                 
                  <div class='d-flex  justify-content-between'>
                   <span>$user[name] $user[family]</span>
                   <span class='text-blue'>$user[mobile] <i class='fa fa-mobile-alt'></i></span>
                  </div>
                  
                     <div class='d-flex mt-2'>
                   <span class='text-gray'>صنف : </span>
                   <span class='mr-2'>$user[guild_name]</span>
                  </div>
                  <div class='d-flex mt-2'>
                   <span class='text-gray'>موقعیت مکانی : </span>
                   <span class='mr-2'>$user[province_name] / $user[city_name]</span>
                  </div>
                     <div class='d-flex mt-2'>
                   <span class='text-gray'>آدرس : </span>
                   <span class='mr-2'>$user[address]</span>
                  </div>
                  $karfarma
                 
                  <div class='d-flex mt-2 justify-content-end'>
                   <button data-id = '$user[user_id]' class='btn btn-success small-font btn-confirm'>تایید کردن</button>
                   <button data-id = '$user[user_id]' class='btn btn-danger mr-2 small-font btn-reject'>رد کردن</button>
                  </div>
               </div>
        
            </div>";
                        }
                    }
                    ?>
                </div>

            </div>

            </div>  <!--tab 1 description-->
            <!--tab 1 description-->
        </div>


    </div>

<?php
   function karfarma_function($user) {
       if ($user['is_employment']==1) {
          return  "<div class='d-flex mt-2'>
                   <span class='text-gray'>کارفرما : </span>
                   <span class='mr-2'>$user[e_name] $user[e_family]</span>
                  </div>
                  <div class='d-flex mt-2'>
                   <span class='text-gray'>شماره همراه کارفرما : </span>
                   <span class='mr-2'>$user[e_mobile]</span>
                  </div>
                  ";
       }else {
             return   "<div class='d-flex mt-2'>
                   <span class='text-gray'>کارفرما : </span>
                   <span class='mr-2'> - </span>
                  </div>
                  
                  <div class='d-flex mt-2'>
                   <span class='text-gray'>شماره همراه کارفرما : </span>
                   <span class='mr-2'> - </span>
                  </div>";
       }
   }

?>






<div class="modal fade" id="state-modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header d-flex  justify-content-center align-items-center">
                <span class="text-bold" id="modal_tile">درخواست </span>
            </div>
            <!-- Modal body -->
            <div class="modal-body d-flex flex-column mt-2 mb-2">
                 <span id="modal_body" class="text-center"></span>
                <span class="text-center mt-1 align-self-center" id="modal_message"></span>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer pt-3 pb-3 d-flex comment-body">
                <a class="text-danger mr-3 text-bold small-font pr-2 pl-2" data-dismiss="modal"> لغو</a>
                <div class="spinner-border spinner-border-sm myspinner d-none" id="spinner_guild"></div>
                <a class="text-success mr-3 text-bold small-font" id="modal_submit">تایید</a>
            </div>
        </div>
    </div>


</div>


<script src="panel/js/bootstrap.js"></script>
<script src="panel/dist/js/exit.js"></script>
<script src="panel/js/compress.js"></script>
<script src="panel/dist/js/adminlte.js"></script>
<script src="panel/js/jquery.magnific-popup.js"></script>
<link rel="stylesheet" href="panel/css/magnific-popup.css">


<script>

    $(document).ready(function () {

        var token =   "<?php echo $_SESSION["ad_token"]?>"
        var state = 0;
        var user_id = 0;



        $('.btn-confirm').click(function (e) {
            state=2;
            user_id = $(this).attr('data-id');
           setModalInfo("تایید","آیا از تایید این فروشنده مطمعن میباشید ؟")
        });
        $('.btn-reject').click(function (e) {
            state=-1;
            user_id = $(this).attr('data-id');
            setModalInfo("رد کردن درخواست","آیا از رد کردن درخواست این فروشنده مطمعن میباشید ؟")
        });

        $('.btn-document').click(function (e) {
            state=1;
            user_id = $(this).attr('data-id');
            setModalInfo("درخواست مدرک","آیا از ارسال درخواست مدرک به این فروشنده مطمعن میباشید ؟")
        });


        $("#state-modal").on("hidden.bs.modal", function () {
            handleMessage(false,"","")
            hanleLoading(false)
            $('#modal_input').val("")
        });

        function setModalInfo(title,body) {
            $('#modal_tile').text(title);
            $('#modal_body').text(body);
            $('#state-modal').modal('show')

        }


        $('#modal_submit').click(function () {
                hanleLoading(true)
                handleMessage(false,"",false)
                $.ajax({
                    type: "PUT",
                    url:  BASE_API+ "setUserState"
                    ,
                    headers: {
                        "Authorization":"bearer "+ token
                    },
                    data: {
                        'state': state,
                        'user_id' : user_id
                    },
                    complete: function () {
                        hanleLoading(false)
                    },
                    success: function (result, status, xhr) {
                    //    handleMessage(true, result["message"], false
                        handleMessage(false,"","")
                        hanleLoading(false)
                        $('#state-modal').modal('hide')
                        $('#div_'+user_id).hide('slow', function(){ $('#div_'+user_id).remove(); });
                    },

                    error: function (xhr, status, error) {
                        var json = JSON.parse(xhr.responseText);
                        handleMessage(true,json["message"],true)
                    }
                });


        })

        function handleMessage(show,message,error){
           if (error) {
               $('#modal_message').addClass('text-danger').removeClass('text-success')
           }else {
               $('#modal_message').removeClass('text-danger').addClass('text-success')
           }

           if (show) {
               $('#modal_message').text(message)
           }else {
               $('#modal_message').text('')
           }
        }

        function hanleLoading(show) {
           if (show) {
               $('#modal_submit').addClass('d-none')
               $('#spinner_guild').removeClass('d-none')
           }else {
               $('#modal_submit').removeClass('d-none')
               $('#spinner_guild').addClass('d-none')
           }
        }

        $('.document').click(function () {
            var document = $(this).attr('data-document')
            // if the document is not empty or nulll
            if (document) {
                $(this).magnificPopup({
                    items: {
                        src: IMAGE_DOCUMENT+document
                    },
                    type: 'image' // this is default type
                });
            }

        })




        $('ul.tabs li').click(function () {
            var tab_id = $(this).attr('data-tab');
            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');
            $(this).addClass('current');
            $("#" + tab_id).addClass('current');

        })



    })

</script>

<!- AdminLTE App -->



</body>
</html>
