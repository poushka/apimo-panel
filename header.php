<?php
require __DIR__.'/helper/init.php';
require __DIR__.'/helper/Practical.php';


if (!isset($_SESSION["ad_login"])) {
    header('Location:'.LOGIN_URL);
}
$token = "bearer ".$_SESSION["ad_token"];

$response_guilds = $client->request('GET', 'getGuilds', [
    'headers' => [
        'Authorization' => $token
    ],
]);
$data = json_decode($response_guilds->getBody(), true);
$guilds = $data["data"];


$response = $client->request('GET', 'dashbord/0',[
    'headers' =>[
        'Authorization' => $token
    ],
    'query'=>['simple'=>'yes']
]);
$data = json_decode($response->getBody(), true);
$dashbord = $data["data"];


$current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$current = substr($current_url, strrpos($current_url, '/') + 1);
$current = strtok($current, '?');



if (strpos($current, 'guild_id') !== false) {
    $current="";
}

      if ($current==""){
          $current_fa= "داشبورد";
      }else if ($current=="guild") {
          $current_fa= "مدیریت اصناف";
      }else if ($current=="pending") {
          $current_fa= "کاربران در انتظار";
      }else if ($current=="users") {
          $current_fa= "مدیریت کاربران";
      }else
          if($current=="product") {
          $current_fa= "مدیریت محصولات";
      }else if ($current=="setting") {
          $current_fa="تنظیمات";
      }else if ($current=="specials") {
          $current_fa="ویژه شده ها";
      }else if ($current=="support") {
          $current_fa="پشتیبانی";
      }
?>

<!DOCTYPE html>
<html>
<head>

    <base href=<?php echo BASE_URL?>>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>پنل مدیریت |  <?php echo $current_fa?> </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <!-- Ionicons -->

    <link rel="stylesheet" href="panel/awsome/css/all.min.css">


    <!-- Theme style -->
    <link rel="stylesheet" href="panel/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="panel/css/bootstrap.css">
    <!-- iCheck -->
    <!-- bootstrap rtl -->
    <link rel="stylesheet" href="panel/dist/css/bootstrap-rtl.min.css">
    <!-- template rtl version -->
    <link rel="stylesheet" href="panel/dist/css/custom-style.css">

    <link rel="stylesheet" href="panel/css/style.css">

    <link rel="stylesheet" href="panel/css/jtoggler.styles.css">


    <script src="panel/js/config.js"></script>


    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>




    <link rel="stylesheet" type="text/css" href="panel/css/on-off-switch.css"/>


    <script src="panel/plugins/jquery/jquery.min.js"></script>

    <link href="panel/css/jquery.mswitch.css" rel="stylesheet" type="text/css">

    <script src="panel/js/jquery.mswitch.js" type="text/javascript"></script>
    <script src="panel/js/bootbox.all.min.js" type="text/javascript"></script>


</head>

<body class="hold-transition sidebar-mini" style="overflow-x: hidden">

<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand  navbar-light light-gray border-bottom"
         style="position: -webkit-sticky; position: sticky ;  top: 0">
        <!-- Left navbar links -->
        <ul class="navbar-nav d-flex pr-0">

            <li class="nav-item ">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>

            <li class="nav-item ">
                <a href="<?php echo BASE_URL?>" class="nav-link text-bold">پنل مدیریت اپی مو </a>
            </li>

        </ul>


    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">

        <div class="sidebar" style="direction: ltr">
            <div style="direction: rtl">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel  mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img id="img_top" class="img-circle mr-3" alt="User Image"
                             src="panel/images/user.svg">
                    </div>
                    <div >
                        <span  class="d-block mt-2 mt-2 text-white"><?php echo "نیما کشتکار" ?></span>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column pr-0" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->


                        <li class="nav-item ">
                            <a href="panel" class="nav-link <?php if ($current=="") {echo "active";} ?>">
                                <i class="nav-icon  fas fa-tachometer-alt"></i>
                                <p>
                                    داشبورد
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="panel/guild" id="social-menu" class="nav-link <?php if ($current=="guild") {echo "active";} ?>">
                                <i class="nav-icon fa fa-chart-network"></i>
                                <p>
                                    مدیریت اصناف
                                </p>
                            </a>
                        </li>



                        <li class="nav-item">
                            <a href="panel/pending" id="social-menu" class="nav-link <?php if ($current=="pending") {echo "active";} ?>">
                                <i class="nav-icon fa fa-spinner"></i>
                                <p>
                                    کاربران در انتظار
                                    <span class="right badge badge-danger"> <?php echo $dashbord["pending"]
                                        ?> </span>
                                </p>


                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="panel/users" id="social-menu" class="nav-link <?php if ($current=="users") {echo "active";} ?>">
                                <i class="nav-icon fa fa-users"></i>
                                <p>
                                    مدیریت کاربران

                                </p>


                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="panel/product" id="social-menu" class="nav-link <?php if ($current=="product") {echo "active";} ?>">
                                <i class="nav-icon fa fa-inventory"></i>
                                <p>
                                    مدیریت محصولات
                                </p>
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="panel/setting" id="social-menu" class="nav-link <?php if ($current=="setting") {echo "active";} ?>">
                                <i class="nav-icon fa fa-cog"></i>
                                <p>
                                    تنظیمات
                                </p>
                            </a>
                        </li>


                        <li class="nav-item" data-function="exit">
                            <a href="#" class="nav-link d-flex align-items-center">
                                <i class="nav-icon fa fa-sign-out "></i>
                                <p>خروج از حساب </p>
                            </a>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
        </div>
        <!-- /.sidebar -->
    </aside>