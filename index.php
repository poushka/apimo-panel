<?php
include 'header.php';

$response_guilds = $client->request('GET', 'getGuilds', [
    'headers' => [
        'Authorization' => $token
    ],
]);
$data = json_decode($response_guilds->getBody(), true);
$guilds = $data["data"];


if (count($guilds)>0) {
    if (isset($_GET["guild_id"])) {
        $guild_id=$_GET["guild_id"];
    }else {
        $guild_id = $guilds[0]["guild_id"];
    }

    $response_dashbord = $client->request('GET', "dashbord/$guild_id", [
        'headers' => [
            'Authorization' => $token
        ],
          'query'=>['simple'=>'no']
    ]);
    $data = json_decode($response_dashbord->getBody(), true);
    $dashbord  = $data["data"];

}


?>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <div class="container-fluid d-flex justify-content-between  align-items-center m mt-2 mr-2 mt-md-2 mr-md-2">
        <h1 class="my-h1 mr-2">داشبورد</h1>
    </div>
    <!-- Content Header (Page header) -->
    <div class="d-flex flex-column  mt-1 mr-md-4 mr-2">

        <div class="d-flex align-items-center">
            <select class="form-control" style="min-width: 200px; max-width: 300px" id="select_product">
                <?php
                foreach ($guilds as $guild) {
                    if ($guild['guild_id']==$guild_id) {
                        echo "<option selected='selected'  data-id='$guild[guild_id]'>$guild[name]</option>";
                    }else {
                        echo "<option data-id='$guild[guild_id]'>$guild[name]</option>";
                    }
                }
                ?>
            </select>
        </div>

    </div>





    <div class="container p-2 ">

        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6 p-1 pr-2">
                <!-- small box -->
                <div class="networks-item bg-gray d-flex flex-column p-3">
                        <h3 class="text-center mt-3"><?php echo $dashbord['all_users'] ?></h3>
                        <p class="text-bold text-center">تعداد کل کاربران</p>
                </div>
            </div>

            <div class="col-lg-3 col-6 p-1 pr-2">
                <!-- small box -->
                <div class="networks-item d-flex flex-column p-3">
                    <h3 class="text-center mt-3"><?php echo $dashbord['24_hours_users'] ?></h3>
                    <p class="text-bold text-center">کاربران 24 ساعت اخیر</p>
                </div>
            </div>

            <div class="col-lg-3 col-6 p-1 pr-2">
                <!-- small box -->
                <div class="networks-item bg-gray d-flex flex-column p-3">
                    <h3 class="text-center mt-3"><?php echo $dashbord['guild_users'] ?></h3>
                    <p class="text-bold text-center"><?php echo "کاربران صنف " . $dashbord['guild_name']?></p>
                </div>
            </div>




            <div class="col-lg-3 col-6 p-1 pr-2">
                <!-- small box -->
                <div class="networks-item d-flex flex-column p-3">
                    <h3 class="text-center mt-3"><?php echo $dashbord['24_guild_users'] ?></h3>
                    <p class="text-bold text-center">کاربران 24 ساعت صنف</p>
                </div>
            </div>


            <div class="col-lg-3 col-6 p-1 pr-2">
                <!-- small box -->
                <div class="networks-item bg-gray d-flex flex-column p-3">
                    <h3 class="text-center mt-3"><?php echo $dashbord['all_ads'] ?></h3>
                    <p class="text-bold text-center">تعداد تمامی آگهی ها</p>
                </div>
            </div>


            <div class="col-lg-3 col-6 p-1 pr-2">
                <!-- small box -->
                <div class="networks-item d-flex flex-column p-3">
                    <h3 class="text-center mt-3"><?php echo $dashbord['24_hours_ads'] ?></h3>
                    <p class="text-bold text-center">آگهی های 24 ساعت </p>
                </div>
            </div>


            <div class="col-lg-3 col-6 p-1 pr-2">
                <!-- small box -->
                <div class="networks-item bg-gray d-flex flex-column p-3">
                    <h3 class="text-center mt-3"><?php echo $dashbord['all_guild_ads'] ?></h3>
                    <p class="text-bold text-center"><?php echo "اگهی های صنف " .$dashbord['guild_name'] ?></p>
                </div>
            </div>

            <div class="col-lg-3 col-6 p-1 pr-2">
                <!-- small box -->
                <div class="networks-item d-flex flex-column p-3">
                    <h3 class="text-center mt-3"><?php echo $dashbord['24_hours_ads'] ?></h3>
                    <p class="text-bold text-center">اگهی های 24 ساعت صنف </p>
                </div>
            </div>


        </div>
















    <!-- /.row (main row) -->
</div><!-- /.container-fluid -->


    <div class="row">
        <div class="col-lg-10 offset-lg-1">

    <canvas id="myChart" ></canvas>
        </div>
    </div>



<!-- /.content -->


</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery -->
<script src="panel/plugins/jquery/jquery.min.js"></script>
<script src="panel/dist/js/exit.js"></script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="panel/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script>

    $(document).ready(function () {

        var tokenn = "<?php echo $_SESSION["ad_token"]?>"
        var $guild_id = "<?php echo $guild_id?>"

        $('#select_product').on('change',function (){
            var guild_id = $(this).find('option:selected').attr('data-id');
            window.location.href = PANEL_URL +"?guild_id="+guild_id;
        });

        $.ajax({
            type: "GET",
            url:  BASE_API+ "agahiChart/"+$guild_id
            ,
            headers: {
                "Authorization":"bearer "+ tokenn
            },
            complete: function () {
            },
            success: function (result, status, xhr) {
                var my_data  = result["data"]
                var guild_name = "<?php echo $dashbord['guild_name']?>"

                var labels = [];
                var chart_data = [];

                my_data.forEach(function (item) {
                    labels.push(item["hour"])
                    chart_data.push(item["count"])
                })

                for(var i =0 ; i<=24 ; i++) {
                    if(jQuery.inArray(i, labels) === -1) {
                        labels.splice(i,0,i)
                        chart_data.splice(i,0,0)
                    }
                }




                const data = {
                    labels: labels,
                    datasets: [{
                        label: 'نمودار ساعتی اگهی های ' + guild_name,
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(217,9,98)',
                        data: chart_data,
                    }]
                };
                const config = {
                    type: 'line',
                    data,
                    options: {}
                };

                var myChart = new Chart(
                    document.getElementById('myChart'),
                    config
                );
            },

            error: function (xhr, status, error) {
                console.log()
            }
        });

    })

</script>

<script>


</script>

</body>
</html>
