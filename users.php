<?php
use Src\helper\Practical;
include 'header.php';

$page = 1;

if (isset($_GET["page"])) {
    $page = $_GET["page"];
}
$search=null;

if (isset($_GET["search"])) {
    $search=$_GET["search"];
}


$response_guilds = $client->request('GET', "getUsers", [
    'headers' => [
        'Authorization' => $token
    ],
    'query' => ['page' => $page,'search'=>$search]
]);
$data = json_decode($response_guilds->getBody(), true);
$users = $data["data"]["data"];
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-4 d-flex align-items-center justify-content-start mt-3">
            <input id="et_search" class="form-control h-100 ml-2 mr-2  flex-grow-1 small-font" type="search"
                   placeholder= "<?php echo  "جستجو شماره موبایل ، نام کاربر ، نام فروشگاه" ?>"  aria-label="Search">
            <button id="btn_search" class="btn btn-success ml-2 ml-sm-0">جستجو</button>

        </div>

    </div>

    <div  class="container-fluid  p-xl-3 p-2" id="div_messages">

        <?php

        $search_result = null;
        if ($search!=null) {
            echo "<div class='align-items-center justify-content-center align-self-start  d-flex'>
          <span class='text-bold'>نتایج جتسجوی $search</span>
          <span id='tv_remove_search' class='my-red pointer mr-2 small-font'> حذف جستجو</span>
           </div>";
        }
        ?>

        <?php
        if (count($users) == 0) {
            echo Practical::printEmpty("هیچ کاربری یافت نشد");
        }
        ?>
        <div class="table-responsive"  id="product_row">

            <table class="table rtl table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">نام</th>
                    <th scope="col">نام خانوادگی</th>
                    <th scope="col">موبایل</th>
                    <th scope="col">فروشگاه</th>
                    <th scope="col">استخدام</th>
                    <th scope="col">استان</th>
                    <th scope="col">شهر</th>
                    <th scope="col">آدرس</th>
                    <th scope="col">تلفن 1</th>
                    <th scope="col">تلفن 2</th>
                    <th scope="col">صنف</th>
                    <th scope="col">تاریخ ثبت نام</th>
                    <th scope="col"> آگهی ها</th>
                    <th scope="col">وضعیت</th>
                </tr>
                </thead>
                <tbody class="small-font">
                <?php
                $counter = 1;
                foreach ($users as $user) {
                    $estekhdam = $user['is_employment'] ? "خیر" : "بلی";
                    $tel_1 = $user['tel_1'] == null ? "__" : $user["tel_1"];
                    $tel_2 = $user['tel_2'] == null ? "__" : $user["tel_2"];
                    $active = $user['active']==1? "مسدود کردن" : "فعال سازی";
                    echo "    <tr>
                    <th scope='row'>$counter</th>
                    <td>$user[name]</td>
                    <td>$user[family]</td>
                    <td>$user[mobile]</td>
                     <td>$user[store_name]</td>
                     <td>$estekhdam</td>
                     <td>$user[province_name]</td>
                     <td>$user[city_name]</td>
                     <td>$user[address]</td>
                     <td>$tel_1</td>
                     <td>$tel_2</td>
                     <td>$user[guild_name]</td>    
                     <td>$user[shamsi]</td>     
                     <td><a href='#'>نمایش</a></td> 
                     <td class='pointer activate my-red' data-id='$user[user_id]' data-active='$user[active]'>$active</td> 
                     </tr>";
                    $counter++;
                }

                ?>
                </tbody>
            </table>


        </div>

    </div><!-- /.container-fluid -->

<!--    pagging div-->
    <div class="d-flex justify-content-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <?php
                if ($page > 1) {
                    $preve_page_index = $page - 1;
                    $first_page = PANEL_URL . "users?page=1";
                    $preve_page = PANEL_URL . "users?page=$preve_page_index";

                    echo "<li class='page-item'><a class='page-link' href='$first_page'>اولین</a></li>
                      <li class='page-item'>
                <a class='page-link' href='$preve_page' aria-label='Previous'>
                    <span aria-hidden='true'>&laquo;</span>
                    <span class='sr-only'>قبلی</span>
                </a>
               </li>";
                }
                if ($data["data"]["last_page"] > 1) {
                    for ($i = 1; $i <= $data["data"]["last_page"]; $i++) {
                        $page_address = PANEL_URL . "users?page=" . $i;
                        if ($page == $i) {
                            $myclass = "page-item active";
                        } else {
                            $myclass = "page-item";
                        }
                        echo " <li class='$myclass'><a class='page-link' href='$page_address'>$i</a></li>";
                    }
                }

                if ($page != $data["data"]["last_page"]) {
                    $last_page = PANEL_URL . "users?page=" . $data["data"]["last_page"];
                    $next_page_index = $page + 1;
                    $next_page = PANEL_URL . "users?page=$next_page_index";
                    echo "        <li class='page-item'>
                <a class='page-link' href='$next_page' >
                    <span aria-hidden='true'>&raquo;</span>
                    <span class='sr-only'>بعدی</span>
                </a>
            </li>
            
            <li class='page-item'><a class='page-link' href='$last_page'>آخرین</a></li>";
                }
           ?>
            </ul>
        </nav>
    </div>

</div>
<script src="panel/js/bootstrap.js"></script>
<script src="panel/dist/js/exit.js"></script>
<script src="panel/js/compress.js"></script>
<script src="panel/dist/js/adminlte.js"></script>
<script src="panel/js/jquery.magnific-popup.js"></script>
<link rel="stylesheet" href="panel/css/magnific-popup.css">

<script>

    $(document).ready(function () {
        var tokenn = "<?php echo $_SESSION["ad_token"]?>"
        $('.activate').click(function () {
          var user_id = $(this).attr('data-id');
          var active = $(this).attr('data-active');
          var active= active==1? 0 : 1;

            bootbox.confirm("آیا از انجام این عملیات مطمعن میباشید", function(result){
                  if (result) {
                      $.ajax({
                          type: "PUT",
                          url:  BASE_API+ "activateUser/"
                          ,
                          headers: {
                              "Authorization":"bearer "+ tokenn
                          },
                          data: {
                            "user_id":user_id,
                            "active": active
                          },
                          complete: function () {
                          },
                          success: function (result, status, xhr) {
                              location.reload();
                          },

                          error: function (xhr, status, error) {
                              alert("خطا در به روز رسانی . دوباره تلاش کنید")
                          }
                      });
                  }
            })
        })

        $('#tv_remove_search').click(function (){
            var newUrl = PANEL_URL+"users";
            window.location.href = newUrl
        })


        $("#btn_search").click(function () {
            var search = $('#et_search').val()
            if (search.trim().length==0) {
                alert("لطفا مقدار جستجو را وارد نمایید")
                return
            }
            var newUrl = PANEL_URL+"users?search="+search;
            window.location.href = newUrl

        })


    })

</script>


</body>
</html>
