<?php

use Src\helper\Practical;

include 'header.php';
$response_guilds = $client->request('GET', 'getGuilds', [
    'headers' => [
        'Authorization' => $token
    ],
]);
$data = json_decode($response_guilds->getBody(), true);
$guilds = $data["data"];

$page=1;
if (isset($_GET["page"])) {
    $page=$_GET["page"];
}

$search=null;

if (isset($_GET["search"])) {
    $search=$_GET["search"];
}

if (count($guilds)>0) {
    if (isset($_GET["guild_id"])) {
        $guild_id=$_GET["guild_id"];
    }else {
        $guild_id = $guilds[0]["guild_id"];
    }
    $response_guilds = $client->request('GET', "getProducts/$guild_id", [
        'headers' => [
            'Authorization' => $token
        ],
        'query'=>['page'=>$page,'search'=>$search]
    ]);
    $data = json_decode($response_guilds->getBody(), true);
    $products  = $data["data"]["data"];
}


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="d-flex flex-column  mt-3 mr-md-4 mr-2">
        <label class="" for="exampleFormControlSelect1">انتخاب صنف</label>
        <div class="d-flex align-items-center">
        <select class="form-control" style="min-width: 200px; max-width: 300px" id="select_product">
            <?php
            foreach ($guilds as $guild) {
                if ($guild['guild_id']==$guild_id) {
                    echo "<option selected='selected'  data-id='$guild[guild_id]'>$guild[name]</option>";
                }else {
                    echo "<option data-id='$guild[guild_id]'>$guild[name]</option>";
                }
            }
            ?>
        </select>
        <p   id="new_product" class=" btn btn-success d-flex align-items-center mr-2 mt-3 pointer"> <i class="fa fa-plus ml-1"></i> افزودن  </p>
        </div>


        <div class="row">
            <div class="col-md-4 d-flex align-items-center justify-content-start mt-3">
                <input id="et_search" class="form-control h-100 ml-2 mr-2  flex-grow-1 small-font" type="search"
                       placeholder= "<?php echo  "جتسجوی نام محصول" ?>"  aria-label="Search">
                <button id="btn_search" class="btn btn-success ml-2 ml-sm-0">جستجو</button>

            </div>

        </div>

    </div>

    <div class="container  p-xl-3 p-2" id="div_messages">

        <?php

        $search_result = null;
        if ($search!=null) {
            echo "<div class='align-items-center justify-content-center align-self-start  d-flex'>
          <span class='text-bold'>نتایج جتسجوی $search</span>
          <span id='tv_remove_search' class='my-red pointer mr-2 small-font'> حذف جستجو</span>
           </div>";
        }
        ?>

        <?php
           if (count($products)==0) {
           echo  Practical::printEmpty("هیچ محصولی یافت نشد");
           }
        ?>
        <div class="row products" id="product_row">
            <?php
            foreach ($products as $product) {
                $id = "p_".$product["product_id"];
                $img =  IMAGE_PRODUCT . $product["pic"];
                $count = 1;
                echo "<div class='main_container col-md-6 d-flex mt-2 pt-2 pb-2' id='div_$product[product_id]'>
                          
                           <div class='d-flex flex-column w-100'>
                        
                           <div class='d-flex align-items-center '>
                          <span class='mr-2 text-bold' id='$id'>$product[name]</span>
                          <div class='d-flex align-items-center flex-1 justify-content-end'>
                           <i class='update-product fa fa-pen pointer ml-3' data-name='$product[name]' data-id='$product[product_id]' data-pic='$product[pic]' ></i>
                              <i class='delete_product text-danger fa fa-trash pointer ml-3'  data-id='$product[product_id]'></i>
                                </div>
                                      <img class='pointer img-product' data-pic='$product[pic]' src='$img' width='30' height='30' style='border-radius: 4px'>
                        </div>
                         <hr style='height: 2px; width: 100%'/>
                        </div>
                     </div>
                   
                     ";
                $count++;
            }
            ?>
        </div>

         </div><!-- /.container-fluid -->

    <div class="d-flex justify-content-center">

<!--     --><?php
//
//       if ($products["data"]["last_page"]>1) {
//           echo
//       }
//
//     ?>



     <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php
            if ($page>1) {
                $preve_page_index= $page-1;
                $first_page = PANEL_URL."product?guild_id=$guild_id&page=1";
                $preve_page = PANEL_URL."product?guild_id=$guild_id&page=$preve_page_index";

                echo  "<li class='page-item'><a class='page-link' href='$first_page'>اولین</a></li>
                      <li class='page-item'>
                <a class='page-link' href='$preve_page' aria-label='Previous'>
                    <span aria-hidden='true'>&laquo;</span>
                    <span class='sr-only'>قبلی</span>
                </a>
               </li>";
            }

            if ($data["data"]["last_page"]>1) {
                for ($i=1 ; $i<=$data["data"]["last_page"];$i++) {
                    $page_address = PANEL_URL."product?guild_id=$guild_id&page=".$i;
                    if ($page==$i) {
                        $myclass ="page-item active";
                    }else {
                        $myclass ="page-item";
                    }
                    echo " <li class='$myclass'><a class='page-link' href='$page_address'>$i</a></li>";
                }
            }

            if ($page!=$data["data"]["last_page"]) {
                $last_page = PANEL_URL."product?guild_id=$guild_id&page=".$data["data"]["last_page"];
                $next_page_index= $page+1;
                $next_page = PANEL_URL."product?guild_id=$guild_id&page=$next_page_index";
                echo " <li class='page-item'>
                <a class='page-link' href='$next_page' >
                    <span aria-hidden='true'>&raquo;</span>
                    <span class='sr-only'>بعدی</span>
                </a>
            </li>
            <li class='page-item'><a class='page-link' href='$last_page'>آخرین</a></li>";
            }
            ?>

        </ul>


    </nav>

    </div>

</div>

<!-- /.content -->


<!-- ./wrapper -->
<div class="modal fade" id="product_modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header d-flex  justify-content-start mr-2 align-items-center">
                <span class="text-bold text-right" id="modal_tile">افزودن محصول جدید</span>
            </div>

            <!-- Modal body -->
            <div class="modal-body d-flex flex-column mt-2 mb-2">
                <span> نام محصول</span>
                <input class="input-modal mt-1" id="modal_input" type="text" placeholder="">

                <span class="mt-3"> تصویر محصول</span>
                <div class="image-upload align-self-start mt-2">
                    <!--     <label for="img-input">
                            </label>-->
                    <img src="panel/assets/gallery.png"  id="img_page" width="110" height="110""/>
                    <input id="img-input" type="file" accept="image/*"
                           data-type='image'/>
                </div>

                <span class="text-center mt-1 align-self-center" id="modal_message"></span>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer pt-3 pb-3 d-flex comment-body">
                <a class="text-danger mr-3 text-bold small-font pr-2 pl-2" data-dismiss="modal"> لغو</a>
                <div class="spinner-border spinner-border-sm myspinner d-none" id="spinner_guild"></div>
                <a class="text-success mr-3 text-bold small-font" id="modal_submit">ذخیره کردن  </a>


            </div>

        </div>
    </div>


</div> <!--comment_modal-->

<div class="modal fade" id="state-modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header d-flex  justify-content-center align-items-center">
                <span class="text-bold" id="modal_tile_state">درخواست </span>
            </div>
            <!-- Modal body -->
            <div class="modal-body d-flex flex-column mt-2 mb-2">
                <span id="modal_body_state" class="text-center"></span>
                <span class="text-center mt-1 align-self-center" id="modal_message_state"></span>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer pt-3 pb-3 d-flex comment-body">
                <a class="text-danger mr-3 text-bold small-font pr-2 pl-2" data-dismiss="modal"> لغو</a>
                <div class="spinner-border spinner-border-sm myspinner d-none" id="spinner_state"></div>
                <a class="text-success mr-3 text-bold small-font" id="modal_submit_state">تایید</a>
            </div>
        </div>
    </div>


</div>


<!-- jQuery -->




<script src="panel/js/bootstrap.js"></script>
<script src="panel/dist/js/exit.js"></script>
<script src="panel/js/compress.js"></script>
<script src="panel/dist/js/adminlte.js"></script>
<script src="panel/js/jquery.magnific-popup.js"></script>
<link rel="stylesheet" href="panel/css/magnific-popup.css">

<script>

    $(document).ready(function () {

        var tokenn = "<?php echo $_SESSION["ad_token"]?>"
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];

        var isNew = true ;
        var product_id_update = 0;
        var image_in_update ="";
        var delete_id = 0;
        var myFormData = new FormData();



        $('#select_product').on('change',function (){
            var guild_id = $(this).find('option:selected').attr('data-id');
            window.location.href = PANEL_URL+"product?guild_id="+guild_id;
        });

        $("#new_product").click(function () {
            isNew=true
            setModalInfo(isNew,"ایجاد محصول جدید" ,"","افزودن")

        })
        //open file choser when image is clicked
        $('#img_page').click(function () {
            $('#img-input').trigger('click');
        })

        //handling what happens after img-input is selected
        $("#img-input").change(function () {
            //  var formData = new FormData(this);
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                    new Compressor(input.files[0], {
                        quality: .8,
                        maxWidth : 1200,
                        maxHeight :1200,
                        convertSize : 50000,
                        success(result) {
                            $('#img_page').attr('src', window.URL.createObjectURL(result));
                            myFormData.append('pic', result, result.name)
                            console.log(myFormData)
                        },
                        error(e) {
                            console.log(e.message);
                        },
                    });


                } else {
                    // swal({
                    //     title: "خطا !",
                    //     text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                    //     icon: "warning",
                    //     button: "بسیار خوب ",
                    // });
                }


            }
        }


        $('#modal_submit').click(function () {

                var name = $("#modal_input").val()
                if (name.toString().trim().length==0) {
                    handleMessage(true,"لطفا نام محصول  را وارد نمایید" , true)
                    return
                }
                 myFormData.append('name',name)
                 myFormData.append('guild_id',<?php echo $guild_id?>)

                 if (isNew) {
                     if ($("#img-input").val() == "" || $.inArray($("#img-input").prop('files')[0]["type"], validImageTypes) < 0) {
                         handleMessage(true,"لطفا ابتدا تصویر را انتخاب نمایید" ,true)
                         return
                     }
                     hanleLoading(true)
                     handleMessage(false,"",false)
                     $.ajax({
                         type: "POST",
                         processData: false, // important
                         contentType: false, // important
                         url:  BASE_API+ "makeProduct"
                         ,

                         headers: {
                             "Authorization":"bearer "+ tokenn
                         },
                         data:myFormData,

                         complete: function () {
                             hanleLoading(false)
                         },
                         success: function (result, status, xhr) {
                             handleMessage(true,"محصول جدید با موفقیت اضافه شد" , false)
                             setTimeout(function () {
                                 $('#product_modal').modal('hide');
                                 location.reload();
                             },1500)
                         },

                         error: function (xhr, status, error) {
                             var json = JSON.parse(xhr.responseText);
                             handleMessage(true,json["message"],true)
                         }
                     });
                 }   else {

                     hanleLoading(true)
                     handleMessage(false,"",false)
                     $.ajax({
                         type: "POST",
                         processData: false, // important
                         contentType: false, // important
                         url:  BASE_API+ "updateProduct/"+product_id_update
                         ,

                         headers: {
                             "Authorization":"bearer "+ tokenn
                         },
                         data:myFormData,
                         complete: function () {
                             hanleLoading(false)
                         },
                         success: function (result, status, xhr) {
                             handleMessage(true,"به روز رسانی با موفقیت انجام شد" , false)
                             setTimeout(function () {
                                 $('#product_modal').modal('hide');
                                 location.reload();
                             },1500)
                         },
                         error: function (xhr, status, error) {
                             var json = JSON.parse(xhr.responseText);
                             handleMessage(true,json["message"],true)
                         }
                     });

                 }





        })




        $('.main_container').on('click','.delete_product',function (e) {
            delete_id=$(this).attr('data-id')
            setSateModalInfo('حذف محصول','آایا از حذف این محصول اطمینان دارید ؟')

        });

        $('.main_container').on('click','.update-product',function (e) {
            isNew=false;
            var name = $(this).attr('data-name')
            product_id_update = $(this).attr('data-id')
            image_in_update = $(this).attr('data-pic')
            setModalInfo(false,"به روز رسانی " + name ,name,"به روز رسانی")
        });


        $('#modal_submit_state').click(function () {
             loadingState(true)

            $.ajax({
                type: "DELETE",
                url:  BASE_API+ "deleteProduct/"+delete_id
                ,
                headers: {
                    "Authorization":"bearer "+ tokenn
                },
                complete: function () {
                },
                success: function (result, status, xhr) {
                    $('#state-modal').modal('hide')
                    $('#div_'+delete_id).hide('slow', function(){ $('#div_'+delete_id).remove(); });
                },

                error: function (xhr, status, error) {
                    alert("خطا در به حذف ! دوباره تلاش کنید")
                }
            });

        })

        function setSateModalInfo(title,body) {
            $('#modal_tile_state').text(title);
            $('#modal_body_state').text(body);
            $('#state-modal').modal('show')
        }
        function loadingState(show) {
            if (show) {
                $('#modal_submit_state').addClass('d-none')
                $('#spinner_state').removeClass('d-none')
            }else {
                $('#modal_submit_state').removeClass('d-none')
                $('#spinner_state').addClass('d-none')
            }
        }

        $("#state-modal").on("hidden.bs.modal", function () {
            loadingState(false)
        });

        $('.img-product').click(function () {
            var pic = $(this).attr('data-pic')
            // if the document is not empty or nulll
                $(this).magnificPopup({
                    items: {
                        src: IMAGE_PRODUCT+pic
                    },
                    type: 'image' // this is default type
                });

        })


        $("#btn_search").click(function () {
              var guild_id = "<?php echo $guild_id ?>";
              var search = $('#et_search').val()
              if (search.trim().length==0) {
                  alert("لطفا مقدار جستجو را وارد نمایید")
                  return
              }
            var newUrl = PANEL_URL+"product?guild_id="+guild_id+"&search="+search;
            window.location.href = newUrl

        })


        $('#tv_remove_search').click(function (){
            var guild_id = "<?php echo $guild_id ?>";
          var newUrl = PANEL_URL+"product?guild_id="+guild_id;
           window.location.href = newUrl
        })


        $("#product_modal").on("hidden.bs.modal", function () {
            handleMessage(false,"","")
            hanleLoading(false)
            $('#modal_input').val("")
            $("#img-input").val(null);
        });

        function setModalInfo(isNew,title,text,submit) {
            $('#modal_tile').text(title)
            if (isNew) {
                $('#modal_input').attr('placeholder' , "")
                $('#img_page').attr('src','panel/assets/gallery.png')
            }else {
                $('#modal_input').val(text)
                $('#img_page').attr('src', IMAGE_PRODUCT+image_in_update)
            }
            $('#modal_submit').text(submit)
            $('#product_modal').modal('show')

        }

        function handleMessage(show,message,error){
            if (error) {
                $('#modal_message').addClass('text-danger').removeClass('text-success')
            }else {
                $('#modal_message').removeClass('text-danger').addClass('text-success')
            }

            if (show) {
                $('#modal_message').text(message)
            }else {
                $('#modal_message').text('')
            }
        }

        function hanleLoading(show) {
            if (show) {
                $('#modal_submit').addClass('d-none')
                $('#spinner_guild').removeClass('d-none')
            }else {
                $('#modal_submit').removeClass('d-none')
                $('#spinner_guild').addClass('d-none')
            }
        }


    })

</script>


</body>
</html>
