<?php

use Src\helper\Practical;

include 'header.php';
$response_setting = $client->request('GET', 'setting', [
    'headers' => [
        'Authorization' => $token
    ],
]);
$data = json_decode($response_setting->getBody(), true);
$setting = $data["data"];

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="d-flex flex-column  mt-3 mr-md-4 mr-2">
        <label class="" for="exampleFormControlSelect1">ویرایش تنظیمات</label>


    <div class="container  p-xl-3 p-2" id="div_setting">

        <div class="row products" id="product_row">
            <div class="col-md-8  d-flex flex-column justify-content-start align-items-start">

<!--                <div class="d-flex align-self-stretch mt-4 align-items-center justify-content-between">-->
<!--                    <span class="small-font text-gray">اجازه ثبت آگهی</span>-->
<!--                    <input type='checkbox' id='mycheck' class='m_switch_check ml-2' value="--><?php //echo $setting["open"] ?><!--" >-->
<!--                </div>-->


                <span class="small-font mt-3 text-gray">محدودیت ثبت آگهی (دقیقه)</span>
                <input type="number"   value="<?php echo $setting["ad_limit"] ?>" id="input_limit"
                       class="form-control ltr  mt-1">

                <span class="mt-3 small-font text-gray">زمان منسوخ شدن آگهی (روز)</span>
                <input type="number"   value="<?php echo $setting["expire"] ?>" id="input_expire"
                       class="form-control ltr  mt-1">

                <span class="mt-3 small-font text-gray">متن درباره ما</span>
                <textarea class="form-control  small-font w-100" rows="6" id="input_about"
                          ><?php echo $setting["about"] ?></textarea>

                <span class="mt-3 small-font text-gray">متن قوانین</span>
                <textarea class="form-control  small-font w-100" rows="6" id="input_rules"
                ><?php echo $setting["rules"] ?></textarea>


                <button id="btn_save" class="btn  btn-success mt-3 align-self-end" type="button">ذخیره تغییرات</button>


            </div>

        </div>

         </div><!-- /.container-fluid -->


    </div>

</div>








<script src="panel/js/bootstrap.js"></script>
<script src="panel/dist/js/exit.js"></script>
<script src="panel/dist/js/adminlte.js"></script>


<script>

    $(document).ready(function () {

        var open = " <?php echo $setting["open"]?> ";
        var tokenn = "<?php echo $_SESSION["ad_token"]?>"
        // $(".m_switch_check:checkbox").mSwitch({
        //     onRender:function(elem){
        //         if (elem.val() == 0){
        //             $.mSwitch.turnOff(elem);
        //         }else{
        //             $.mSwitch.turnOn(elem);
        //         }
        //     },
        //     onRendered:function(elem){
        //         console.log(elem);
        //     },
        //     onTurnOn:function(elem){
        //       open=1
        //
        //     },
        //     onTurnOff:function(elem){
        //      open=0
        //
        //     }
        // });
        $('#btn_save').click(function () {
            var limit = $('#input_limit').val()
            var expire =  $('#input_expire').val()
            var rules =  $('#input_rules').val()
            var about =  $('#input_about').val()
            if (limit.toString().trim().length==0) {
                alert('لطفا محدودیت ثبت آگهی را وارد نمایید')
                return
            }
            if (expire.toString().trim().length==0) {
                alert('لطفا زمان منسوخ شدن آگهی را مشخص نمایید')
                return
            }

            if (rules.toString().trim().length==0) {
                alert('لطفا قوانین را وارد نمایید')
                return
            }
            if (about.toString().trim().length==0) {
                alert('لطفا متن درباره ما را وارد نمایید')
                return
            }

            $.ajax({
                type: "POST",
                url:  BASE_API+ "updateSetting"
                ,
                headers: {
                    "Authorization":"bearer "+ tokenn
                },
                data: {
                    'about': about,
                    'rules':rules,
                    'ad_limit' : limit,
                    'expire':expire,
                    'open':open
                },
                complete: function () {

                },
                success: function (result, status, xhr) {
                  alert(result["message"])
                },

                error: function (xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    alert(json["message"])
                }
            });

        })





    })

</script>


</body>
</html>
