<?php
include 'header.php';
$response_guilds = $client->request('GET', 'getGuilds', [
    'headers' => [
        'Authorization' => $token
    ],
]);
$data = json_decode($response_guilds->getBody(), true);
$guilds = $data["data"];


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <p class="bg_new d-inline-block mt-4 mr-4" id="new_guild"> <i class="fa fa-plus"></i> صنف جدید </p>
    <div class="container  p-xl-3 p-2" id="div_messages">

        <div class="row guilds" id="guild_row">
            <?php
            $count = 1 ;
            foreach ($guilds as $guild) {
                $id = "g_".$guild["guild_id"];
                echo "<div class='guild_container col-md-6 d-flex mt-2 pt-2 pb-2'>
                           <div class='d-flex flex-column w-100'>
                           <div class='d-flex align-items-center justify-content-between'>
                          <span id='$id'>$count -  $guild[name]</span>
                          <div class='d-flex align-items-center'>
                           <i class='update-guild fa fa-pen pointer ml-3' data-name='$guild[name]' data-id='$guild[guild_id]'></i>
                        <input type='checkbox' id='$guild[guild_id]' class='m_switch_check ml-2' value='$guild[active]' >
                        </div>
                        </div>
                         <hr style='  height: 2px; width: 100%'/>
                        </div>
                     </div>
                   
                     ";
                $count++;
            }
            ?>
        </div>

    </div><!-- /.container-fluid -->

</div>

<!-- /.content -->


<!-- ./wrapper -->
<div class="modal fade" id="guild_modal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header d-flex  justify-content-center align-items-center">
                <span class="text-bold" id="modal_tile"> ساخت صنف جدید</span>
            </div>

            <!-- Modal body -->
            <div class="modal-body d-flex flex-column mt-2 mb-2">
                    <input class="input-modal" id="modal_input" type="text" placeholder="نام صنف ">
                <span class="text-center mt-1 align-self-center" id="modal_message"></span>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer pt-3 pb-3 d-flex comment-body">
                <a class="text-danger mr-3 text-bold small-font pr-2 pl-2" data-dismiss="modal"> لغو</a>
                <div class="spinner-border spinner-border-sm myspinner d-none" id="spinner_guild"></div>
                <a class="text-success mr-3 text-bold small-font" id="modal_submit">ذخیره کردن  </a>


            </div>

        </div>
    </div>


</div> <!--comment_modal-->



<!-- jQuery -->




<script src="panel/js/bootstrap.js"></script>
<script src="panel/dist/js/exit.js"></script>
<script src="panel/js/compress.js"></script>
<script src="panel/dist/js/adminlte.js"></script>

<script>

    $(document).ready(function () {
        var isNew = true ;
        var guild_update_id = 0;
        var tokenn = "<?php echo $_SESSION["ad_token"]?>"


        $("#new_guild").click(function () {
            isNew=true
            setModalInfo(true,"افزودن صنف جدید","نام صنف","ذخیره کردن")
        })

        $('.guild_container').on('click','.update-guild',function (e) {
            isNew=false;
            var name = $(this).attr('data-name')
            guild_update_id = $(this).attr('data-id')
            setModalInfo(false,"به روز رسانی " + name ,name,"به روز رسانی")
        });



        $('#modal_submit').click(function () {
            if (isNew) {
                var name = $("#modal_input").val()

                if (name.toString().trim().length==0) {
                    handleMessage(true,"لطفا نام صنف را وارد نمایید" , true)
                    return
                }
                hanleLoading(true)
                handleMessage(false,"",false)

                $.ajax({
                    type: "POST",
                    url:  BASE_API+ "makeGuild"
                    ,
                    headers: {
                        "Authorization":"bearer "+ tokenn
                    },
                    data: {
                        'name': name,
                    },
                    complete: function () {
                        hanleLoading(false)
                    },
                    success: function (result, status, xhr) {
                        handleMessage(true,"صنف جدید با موفقیت اضافه شد" , false)
                        setTimeout(function () {
                            $('#guild_modal').modal('hide');
                             location.reload();
                        },1500)
                    },

                    error: function (xhr, status, error) {
                        var json = JSON.parse(xhr.responseText);
                        handleMessage(true,json["message"],true)
                    }
                });

            }else {
                var name = $("#modal_input").val()
                if (name.toString().trim().length==0) {
                    handleMessage(true,"لطفا نام صنف را وارد نمایید" , true)
                    return
                }
                hanleLoading(true)
                handleMessage(false,"",false)
                $.ajax({
                    type: "PUT",
                    url:  BASE_API+ "updateGuild/"+guild_update_id
                    ,
                    headers: {
                        "Authorization":"bearer "+ tokenn
                    },
                    data: {
                        'name': name,
                    },
                    complete: function () {
                        hanleLoading(false)
                    },
                    success: function (result, status, xhr) {
                        handleMessage(true,"به روز رسانی با موفقیت انجام شد" , false)
                        $('#g_'+guild_update_id).text(name)
                        setTimeout(function () {
                            $('#guild_modal').modal('hide');
                        },1500)
                    },
                    error: function (xhr, status, error) {
                        var json = JSON.parse(xhr.responseText);
                        handleMessage(true,json["message"],true)
                    }
                });


            }

        })


        $("#guild_modal").on("hidden.bs.modal", function () {
            handleMessage(false,"","")
            hanleLoading(false)
            $('#modal_input').val("")
        });

        function setModalInfo(isNew,title,text,submit) {
            $('#modal_tile').text(title)
            if (isNew) {
                $('#modal_input').attr('placeholder' , text)
            }else {
                $('#modal_input').val(text)
            }
            $('#modal_submit').text(submit)
            $('#guild_modal').modal('show')

        }

        function handleMessage(show,message,error){
            if (error) {
                $('#modal_message').addClass('text-danger').removeClass('text-success')
            }else {
                $('#modal_message').removeClass('text-danger').addClass('text-success')
            }

            if (show) {
                $('#modal_message').text(message)
            }else {
                $('#modal_message').text('')
            }
        }

        function hanleLoading(show) {
            if (show) {
                $('#modal_submit').addClass('d-none')
                $('#spinner_guild').removeClass('d-none')
            }else {
                $('#modal_submit').removeClass('d-none')
                $('#spinner_guild').addClass('d-none')
            }
        }




        $(".m_switch_check:checkbox").mSwitch({
            onRender:function(elem){
                if (elem.val() == 0){
                    $.mSwitch.turnOff(elem);
                }else{
                    $.mSwitch.turnOn(elem);
                }
            },
            onRendered:function(elem){
                console.log(elem);
            },
            onTurnOn:function(elem){
                var id = elem.attr('id')
                $.ajax({
                    type: "PUT",
                    url:  BASE_API+ "activateGuild/"+id
                    ,
                    headers: {
                        "Authorization":"bearer "+ tokenn
                    },
                    data: {
                        'active': 1,
                    },
                    complete: function () {
                    },
                    success: function (result, status, xhr) {

                    },

                    error: function (xhr, status, error) {
                        $.mSwitch.turnOff(elem);
                        alert("خطا در به روز رسانی ! دوباره تلاش کنید")
                    }
                });

            },
            onTurnOff:function(elem){

                var id = elem.attr('id')
                $.ajax({
                    type: "PUT",
                    url:  BASE_API+ "activateGuild/"+id
                    ,
                    headers: {
                        "Authorization":"bearer "+ tokenn
                    },
                    data: {
                        'active': 0,
                    },
                    complete: function () {

                    },
                    success: function (result, status, xhr) {
                        console.log("ok")
                    },

                    error: function (xhr, status, error) {
                        var json = JSON.parse(xhr.responseText);
                        $.mSwitch.turnOn(elem);
                        alert("خطا در به روز رسانی ! دوباره تلاش کنید")

                    }
                });
            }
        });

    })

</script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>
