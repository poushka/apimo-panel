<?php

use Src\helper\Practical as MyPractical;

include 'header.php';

$response_pages = $client->request('GET', 'getUserPages?last_id=0', [
    "headers" => [
        "Authorization" => $user["apikey"]
    ]
]);
$pages = json_decode($response_pages->getBody(), true);


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <?php

    if (count($pages)==0) {
        echo "    <div class=\"no_message_div d-flex flex-column align-items-center justify-content-center\">
        <i class=\"text-gray fal fa-times-circle  mr-lg-5\" > </i>
        <p class=\"mt-1 mr-lg-5\">هیچ صفحه یا کانالی ثبت نشده است</p>
        
    </div>";
    }
    ?>

    <div class="container-fluid d-flex flex-wrap justify-content-between  align-items-center p-xl-3 p-2">
        <h1 class="my-h1 mr-md-2">مدیریت صفحات و کانال ها</h1>
    </div>


    <div class="container-fluid d-flex flex-column" id="div_messages">

        <div class="row pages-container">

            <?php
            foreach ($pages as $page) {
                  $src = IMG_URL."chanel_pic/".$page["thumb"];
                  $member = number_format($page["member"]) . " " . $page["member_prefix"];
                  $state = null;
                  $state_color = null;
                  $href=BASE_URL."social/".$page["e_name"]."/".$page["id"];
                  $refresh_id = "ref_".$page["page_id"];
                  $icon_id = "icon_".$page["page_id"];
                  $short_url = BASE_URL."me/".$page["short_url"];
                  $share_message = "لینک کوتاه " . $page["prefix"] . " خود را در اختیار اعضا قرار دهید تا به صفحه شما رای و نظر دهند";

                  if ($page["active"]==-1) {
                      $state="رد شده";
                      $state_color="text-danger";
                  }else if ($page["active"]==-2) {
                      $state="تعلیق شده";
                      $state_color="text-danger";
                  }else if ($page["active"]==0) {
                      $state="در انتظار تایید";
                      $state_color="text-wating";
                  }else  {
                      $state="فعال";
                      $state_color="text-success";
                  }

                echo " <div class=\"col-lg-6 pt-2 pb-2 p-2\" >
              <div id='$page[page_id]' class=\"d-flex flex-column rounded page-card sss\">

              <div class=\"d-flex align-items-center \">
                <a href='$href' target='_blank' class='page-href my-black-color' data-active='$page[active]' data-prefix='$page[prefix]'>
                <img  src=\"$src\" width=\"50px\" height='50px' >
                </a>
                
                      <div class=\"d-flex justify-content-between flex-grow-1 \">
                          <span class=\"text-bold mr-1 $state_color\">$state</span>
                          <span class=\"ml-1 text-bold small-font\">$page[id]</span>
                          
                      </div>
                
                  </div>

              
          
                      <hr class=\"w-100 mt-0 mb-1\"/>
                      
                      <div class='d-flex justify-content-between'>
                       <a  href='$href' target='_blank' class='page-href text-bold my-black-color mr-1' data-active='$page[active]' data-prefix='$page[prefix]'>
                          $page[name]
                         </a>
                         
                               <div class='mt-1 ml-1 d-flex align-items-center'>
                          <span id='$refresh_id' class=\"my-black-color\">$member</span>
                          <i id='$icon_id' data-id='$page[page_id]' title='به روز رسانی' class='fa fa-sync pointer text-green mr-2 ml-2 refresh'></i>
                     </div>
                         
              </div>
                     
                     
                      <span class=\"mt-1 mr-1\">$page[social_name] / $page[cat_name]</span>
                      
                      <span class='mt-3 text-gray  mr-1 ml-1'>$share_message</span>
                      <div class='d-flex justify-content-center align-items-center'>
                      
                       <a class='text-center ml-1 mt-3' href='$short_url'>$short_url</a>
                       <span class='mr-1 mt-3 copy pointer' data-copy='$short_url'>کپی کردن </span>
                    </div>
                     
                
                 
                

          

              <div class=\"d-flex mt-4 page-card-bottom\" style='height: 46px;'>
                  <a data-prefix='$page[prefix]' data-state='$page[active]'  href='dashbord/reserve/$page[page_id]'  class=\"flex-grow-1 d-flex align-items-center justify-content-center page-function bottom-right\"><i class='fa fa-star-shooting ml-1 star-color '></i> ویژه کردن</a>
                  <a data-prefix='$page[prefix]' data-state='$page[active]'  href='dashbord/update/$page[page_id]'  class=\"flex-grow-1 d-flex align-items-center justify-content-center page-function\"><i class='fa fa-redo ml-1'></i> بروزرسانی</a>
                  <a data-prefix='$page[prefix]' data-state='$page[active]'  href='dashbord/banner/$page[page_id]'  class=\"flex-grow-1 d-flex align-items-center justify-content-center page-function top-left\"><i class='fa fa-image ml-1 '></i> ثبت بنر</a>
              </div>

          </div>
        </div>
        
           ";
            }

            ?>



        </div>

        <div class="mt-2  mr-md-4 ml-md-4 d-none justify-content-center align-items-center" id="loading_list"
             style="height: 60px ; background: #e2e2e2 ; border-radius: 12px">
            <span> <div class="spinner-border spinner-border-sm text-muted"></div> در حال دریافت لطفا صبر کنید </span>
        </div>


        <?php
        if (count($pages)>11) {
            echo "     <span id='load_more' class=\"mb-2 d-flex  align-self-center pointer justify-content-center align-items-center more_items_social\">
            <div class=\"spinner-border d-none spinner-border-sm mr-2\"></div>
             نمایش بیشتر <i class='fa fa-arrow-to-bottom mr-2'> </i></span>";
        }

        ?>



    </div><!-- /.container-fluid -->

</div>




<!-- jQuery -->
<script src="dashbord/plugins/jquery/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="dashbord/dist/js/exit.js"></script>
<script src="js/compress.js"></script>

<script>

    $(document).ready(function () {

        var apikey = "<?php echo $user['apikey']?>";
        var myFormData = new FormData();


        $('#btn_save').click(function () {

            var st_id = $('#select_types').find(":selected").attr('id');
            var title = $('#et_title').val();
            var des = $('#et_des').val();
            var mobile = $('#et_mobile').val();
            var yourObject = {
                st_id: st_id,
                title: title,
                message: des,
                mobile: mobile

            }
            myFormData.append('object', JSON.stringify(yourObject));
            showLoading(true)
            showError(false, false, "")
            $.ajax({
                type: "post",
                processData: false, // important
                contentType: false, // important
                dataType: 'json',
                url: baseURl + "support",
                data: myFormData,
                headers: {
                    "Authorization": apikey
                },
                oncomplete: function () {

                    showLoading(false)
                },

                success: function (result, status, xhr) {
                    showLoading(false)
                    showError(true, true, result["message"]);

                },

                error: function (xhr, status, error) {
                    console.log(xhr.responseText)
                    var json = JSON.parse(xhr.responseText);
                    showLoading(false)
                    showError(true, false, json["message"])

                }


            })


        })

        $('#support-img').click(function () {
            $('#img-input').trigger('click');
        })

        //handling what happens after img-input is selected
        $("#img-input").change(function () {
            //  var formData = new FormData(this);
            readURL(this);
        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(input.files[0]['type'], validImageTypes) > 0) {
                    // invalid file type code goes here.

                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#support-img').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                    form_data = input.files[0];

                    //  myFormData.append('pic', input.files[0])

                    new ImageCompressor(input.files[0], {
                        quality: .8,
                        success(result) {

                            myFormData.append('pic', result, result.name)
                            // Send the compressed image file to server with XMLHttpRequest.
                        },
                        error(e) {
                            console.log(e.message);
                        },
                    });


                } else {
                    swal({
                        title: "خطا !",
                        text: "فرمت انتخاب شده مجاز نمیباشد لطفا یک تصویر انتخاب نمایید",
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }


            }
        }

        var loading = false;
        var finished = false;
        var count = <?php  echo count($pages)?>;
        if (count < 12) {
            finished=true;
        }

        $('#load_more').click(function () {
            if (!loading && !finished) {
                $("#loading_list").removeClass("d-none").addClass("d-flex");
                var last_id = $('.page-card:last').attr("id")
                loading=true;
                $.ajax({
                    type: "get",
                    url: baseURl + "getUserPages",
                    headers: {
                        'Authorization': apikey

                    },
                    data: {
                        'last_id': last_id
                    },

                    complete: function () {
                        loading = false;
                        $("#loading_list").removeClass("d-flex").addClass("d-none");
                    },

                    success: function (result, status, xhr) {

                        if (result.length < 12 ) {
                            $('#load_more').removeClass('d-flex').addClass('d-none');
                        }
                        $.each(result, function (index, element) {
                            var message_id = element.message_id;
                            var src = IMAGE_URL+"chanel_pic/"+element.thumb;
                            var state ;
                            var state_color;
                            var href = web_url+"social/"+element.e_name+"/"+element.id;
                            var ref_id = "ref_"+element.page_id;
                            var icon_id = "icon_"+element.page_id;


                            var short_url = web_url+"me/"+element.short_url;
                            var share_message = "لینک کوتاه " + element.prefix + " خود را در اختیار اعضا قرار دهید تا به صفحه شما رای و نظر دهند";


                            if (element.active==-1) {
                                state="رد شده";
                                state_color="text-danger";
                            }else if (element.active==-2) {
                                state="تعلیق شده";
                                state_color="text-danger";
                            }else if (element.active==0) {
                                state="در انتظار تایید";
                                state_color="text-wating";
                            }else  {
                                state="فعال";
                                state_color="text-success";
                            }







                            var div ="<div class='col-lg-6 p-2'>"+
                                "<div id='"+element.page_id+"' class='d-flex flex-column rounded page-card sss'>" +
                                "<div class='d-flex align-items-center'>" +
                                "<a href='"+href+"' data-active='"+element.active+"' class='page-href my-black-color' target='_blank' data-prefix='"+element.prefix+"'>" +
                                "<img src='"+src+"' width='50' height='50' >" +
                                "</a>"+
                                "<div class='d-flex justify-content-between flex-grow-1' >" +
                                "<span class='text-bold mr-1 "+state_color+"'>"+state+"</span>" +
                                "<span class='ml-1 text-bold small-font'>"+element.id+"</span>"+
                                "</div>" +
                                "</div>"+







                                "<hr class='w-100 mt-0 mb-1' />" +
                                "<div class='d-flex justify-content-between'>" +
                                "<a href='"+href+"' data-active='"+element.active+"' class='page-href text-bold mr-1 my-black-color' target='_blank' data-prefix='"+element.prefix+"'>" +
                                ""+element.name+"" +
                                "</a>" +

                                "<div class='mt-1 ml-1 d-flex align-items-center'>" +
                                "<span id='"+ref_id+"' class='my-black-color'>"+element.member.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" "+element.member_prefix+"</span>" +
                                "<i  id='"+icon_id+"' data-id='"+element.page_id+"' class='fa fa-sync pointer mr-2 text-green refresh'></i>" +
                                "</div>"+

                                "</div>"+

                                "<span class='mt-3 mr-1'>"+element.social_name+" / "+element.cat_name+"</span>"+
                                "<span class='mt-3 my-black-color mr-1 ml-1'>"+share_message+"</span>"+
                                "<div class='d-flex justify-content-center align-items-center'>" +
                                "<a href='"+short_url+"' class='text-center ml-3'>"+short_url+"</a>" +
                                "<span data-copy='"+short_url+"' class='copy mr-1 pointer'>کپی کردن</span>"+
                                "</div>"+
                                "<div class='d-flex page-card-bottom mt-3' style='height: 45px;'>" +
                                "<a data-prefix='"+element.prefix+"' data-state='"+element.active+"' href='dashbord/reserve/"+element.page_id+"' class='flex-grow-1 d-flex align-items-center justify-content-center page-function bottom-right'>" +
                                "<i class='fa fa-star-shooting mt-1 ml-1 star-color'></i>ویژه کردن</a>" +
                                "<a data-prefix='"+element.prefix+"' data-state='"+element.active+"' href='dashbord/update/"+element.page_id+"' class='flex-grow-1 d-flex align-items-center justify-content-center page-function'>" +
                                "<i class='fa fa-redo ml-1 mr-1'></i>به روز رسانی</a>" +
                                "<a data-prefix='"+element.prefix+"' data-state='"+element.active+"' href='dashbord/banner/"+element.page_id+"'  class='flex-grow-1 d-flex align-items-center justify-content-center page-function top-left'><i class='fa fa-image ml-1 mr-1'></i> ثبت بنر</a>" +
                                "</div>"
                                +"</div>" +
                                "</div>";

                            $('.pages-container').append(div);



                        });


                    },


                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);

                        var json = JSON.parse(xhr.responseText);
                        if (xhr.status === 404) {
                            finished = true;
                        }


                    }


                });
            }
        })



        $('.pages-container').on('click','.page-href',function (e) {
            var data_active = parseInt($(this).attr("data-active"));
            if (data_active < 1) {
                e.preventDefault();
                var data_prefix = $(this).attr("data-prefix");
                swal({
                    title: "خطا !",
                    text: "مشاهده " +data_prefix + " بعد از تایید " +data_prefix + " امکان پذیر خواهد بود",
                    icon: "warning",
                    button: "بسیار خوب ",
                });
            }
        });


        $('.pages-container').on('click','.page-function',function (e) {
            var active = $(this).attr("data-state");
            var message = "انجام این عملیات پس از تایید " + $(this).attr("data-prefix") + " امکان پذیر است";
            if (active<1) {
                e.preventDefault();
                swal({
                    title: "خطا !",
                    text: message,
                    icon: "warning",
                    button: "بسیار خوب ",
                });
            }
        })


        $('.pages-container').on('click','.copy',function (e) {
             var short_url = $(this).attr('data-copy')
            var dummy = $('<input>').val(short_url).appendTo('body').select()
            document.execCommand('copy')
            dummy.remove();
            $(this).text('کپی شد')
            setTimeout(function () {
                $(this).text('کپی کردن')
            },1500);
        })


        $('.pages-container').on('click','.refresh',function (e) {

            var page_id =$(this).attr('data-id');
            $(this).removeClass('fa fa-sync').addClass('spinner-border spinner-border-sm');
            $.ajax({
                type: "put",
                url: baseURl + "estelamAndUpdate/"+page_id,
                headers: {
                    'Authorization': apikey
                },
                complete: function () {

                },

                success: function (result, status, xhr) {
                    $('#ref_'+result["id"]).text(result["member_count"])
                    $('#icon_'+result["id"]).removeClass('spinner-border spinner-border-sm').addClass('fa fa-sync')
                },


                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                    var json = JSON.parse(xhr.responseText);
                    $('#icon_'+json["id"]).removeClass('spinner-border spinner-border-sm').addClass('fa fa-sync')
                    swal({
                        title: "خطا !",
                        text: json["message"],
                        icon: "warning",
                        button: "بسیار خوب ",
                    });
                }


            });

        });






        function isEmpty(value) {
            return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        }


        function showLoading(show) {
            if (show) {
                $('#btn_save').html("  درحال ارسال\n" +
                    "                         <span class=\"spinner-border spinner-border-sm mr-2\"></span>\n")
            } else {
                $('#btn_save').html("ارسال درخواست")
            }
        }

        function showError(show, success, message) {
            if (show) {
                if (success) {
                    $('#div-message').removeClass('d-none').addClass('success-div').html(message)
                } else {
                    $('#div-message').removeClass('d-none').addClass('error-div').html(message)
                }

            } else {
                $('#div-message').addClass('d-none')
            }
        }


    })

</script>
<!-- jQuery UI 1.11.4 -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!- AdminLTE App -->
<script src="dashbord/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>
